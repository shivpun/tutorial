import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions } from '@angular/router';
import { ProductCreateComponent } from './product/product-create/product-create.component';
import { WeboxappsComponent } from './weboxapps/weboxapps.component';

const routes: Routes = [
  {path: 'product', component: ProductCreateComponent},
  {path: 'weboxapps', children: [
    {path: '', component: WeboxappsComponent}
  ]}
];

const routerOptions: ExtraOptions = {
  useHash: false,
  anchorScrolling: 'enabled',
  // ...any other options you'd like to use
};

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

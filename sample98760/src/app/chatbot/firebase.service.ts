import { Injectable } from '@angular/core';
import { Message } from './message.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { analytics } from 'firebase';
import { User } from './user.model';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  ipAddress = '';
  talkRecord = [];
  faqBotTalkCount = 0;
  faqBotTalk = [];
  faqBotResponse = true;
  faqBotTimeout = 1000;
  faqBotLastTalk = undefined;
  faqBotWriteTalk = false;
  visitorInfo: User;
  visitorInfoList: any = [];
  visitorCollectionTag = 'weboxapps-user';
  constructor(private firestore: AngularFirestore, private http: HttpClient) {
    this.clientIpAddress().subscribe((res: any) => {
      this.ipAddress = res.ip;
    });
    this.fetchTalk('weboxapps-faqBot');
  }

  clientIpAddress(): Observable<any> {
    return this.http.get('http://api.ipify.org/?format=json');
  }

  /* Add Document */
  talk(collection: string, documentId: string, document: any) {
    return new Promise<any>((resolve, reject) => {
    this.firestore.collection(collection).doc(documentId).set(document)
      .then(res => {
        resolve(res);
        console.log('Promise Response: ' + res);
      }, err => reject(err));
    });
  }

  fetchTalk(collection: string) {
    return new Promise<any>((resolve, reject) => {
      this.firestore.collection(collection).snapshotChanges().subscribe(res => {
        res.forEach(rs => {
          const data: any = rs.payload.doc.data();
          if (data.name === 'introduction') {
            this.faqBotTalk.push(this.operatorMessage(data, 'bot'));
          }
          this.talkRecord.push(data);
          this.faqBotResponse = false;
          this.faqBotLastTalk = data;
        });
        console.log(this.talkRecord);
      });
    });
  }

  fetchFaqUserDetail(collection: string, ipAddress: string) {
    return new Promise<any>((resolve, reject) => {
      this.firestore.collection(collection).doc(ipAddress).snapshotChanges().subscribe(res => {
        const data: any = res.payload.data();
        if (data !== undefined && data !== null) {
          this.visitorInfoList = data.users;
          this.visitorInfoList = (this.visitorInfoList === undefined || this.visitorInfoList === null) ? [] : this.visitorInfoList;
        }
      });
    });
  }

  saveFaqUserDetail() {
    const detail: any = {
      users: this.visitorInfoList
    };
    return this.talk(this.visitorCollectionTag, this.ipAddress, detail);
  }

  operatorMessage(data: any, sendId: string) {
    this.faqBotTalkCount = this.faqBotTalkCount + 1;
    return {
      id: this.create_UUID(),
      type: 'text',
      content: data.content,
      ipAddress: this.ipAddress,
      mode: data.mode,
      sender: 'operator',
      visitorResponse: data.visitorResponse,
      senderId: sendId,
      quickReplies: data.quickReplies,
      nextReplies: data.nextReplies,
      count: this.faqBotTalkCount,
      timeSent: new Date().getTime()
    };
  }

  visitorMessage(data: string, sendId: string) {
    this.faqBotTalkCount = this.faqBotTalkCount + 1;
    return {
      id: this.create_UUID(),
      type: 'text',
      content: data,
      ipAddress: this.ipAddress,
      sender: 'visitor',
      senderId: sendId,
      quickReplies: [],
      nextReplies: [],
      count: this.faqBotTalkCount,
      timeSent: new Date().getTime()
    };
  }

  nextMessage(data: string) {
    let msgData: any = null;
    this.talkRecord.forEach(rs => {
      if (rs.name === data) {
        msgData = this.operatorMessage(rs, 'bot');
      }
    });
    return msgData;
  }

  faqNextReplies(reply: any, firstTime: boolean = true) {
    let message: any;
    reply.nextReplies.forEach((rs: any) => {
      message = this.nextMessage(rs);
     });
    if (firstTime) {
      setTimeout(() => {
        this.faqBotResponse = false;
        this.faqAddNextReplies(message);
      }, this.faqBotTimeout);
    } else {
      this.faqAddNextReplies(message);
    }
  }

  faqAddNextReplies(message: any) {
    if (message !== null && message !== undefined) {
      this.faqBotLastTalk = message;
      this.faqBotTalk.push(message);
      this.faqBotWriteTalk = message.mode === 'write' ? false : true;
      if (message.mode === 'read') {
        this.faqNextReplies(message, false);
      }
    }
  }

  faqQuickReplies(quick: any) {
    const message = this.nextMessage(quick.payload);
    setTimeout(() => {
      this.faqBotResponse = false;
      this.faqAddNextReplies(message);
    }, this.faqBotTimeout);
  }

  writeUserDatas(resid: string, document: Message) {
    const msg = {
      id: resid,
      message: [document]
    };
    return new Promise<any>((resolve, reject) => {
    this.firestore.collection('messages').add(msg)
      .then(res => {
        resolve(res);
        console.log('Promise Response: ' + res);
      }, err => reject(err));
    });
  }

  create_UUID(): string {
    let dt = new Date().getTime();
    const uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
        // tslint:disable-next-line: no-bitwise
        const r = ( dt + Math.random() * 16 ) % 16 | 0;
        dt = Math.floor(dt / 16);
        // tslint:disable-next-line: no-bitwise
        return (c === 'x' ? r : (r & 0x3 | 0x8 )).toString(16);
    });
    return uuid;
}

  update(id: string, value: string) {
    return this.firestore.collection('messages').doc(id).set({id, value}).then(res => res);
  }

  messageId(id: string) {
    return this.firestore.collection('messages', ref => ref.where('type', '==' , 'text')).snapshotChanges();
  }
}

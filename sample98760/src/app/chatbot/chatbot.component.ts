import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { AuthService } from './auth.service';
import { FirebaseService } from './firebase.service';
import { User } from './user.model';

@Component({
  selector: 'app-chatbot',
  templateUrl: './chatbot.component.html',
  styleUrls: ['./chatbot.component.scss'],
  animations: [
    trigger('chatZoom', [
      state('normal', style({transform: 'scale(1.1)'})),
      state('w', style({transform: 'scale(0)'})),
      transition('w<=>normal', animate(1000))
    ])
  ],
  encapsulation: ViewEncapsulation.None
})
export class ChatbotComponent implements OnInit {

  state = 'w';
  ipAddress = '';
  talkStarted = false;
  visitorResponse = undefined;
  messageIcon = 'fa-comment-alt';
  bgColor = 'linear-gradient(135deg, rgb(255, 78, 111), rgb(251, 145, 104)); color: rgb(255, 255, 255); width: 111px';
  constructor(private authService: AuthService, private firebaseService: FirebaseService) {
    if (this.authService.currentUserValue) {
      console.log('Currently Login');
    }
    // this.converseService.conversation().forEach(r => this.firebaseService.talk(r.name, r.talk.name, r.talk));
  }

  public get talkCount() {
    const flag = this.firebaseService.talkRecord.length < 1 ? 0 : this.talkStarted ? 0 : 1;
    return flag;
  }

  public get faqBotTalk() {
    return this.firebaseService.faqBotTalk;
  }

  public get faqBotResponse() {
    return this.firebaseService.faqBotResponse;
  }

  public get faqBotWriteTalk() {
    return this.firebaseService.faqBotWriteTalk;
  }

  ngOnInit() {
    console.log('Before login:' + this.authService.currentUserValue);
    // this.authService.login('ABC', 'ABC');
    console.log('After login:' + this.authService.currentUserValue);
  }

  clickFaqBot() {
    if (!this.talkStarted) {
      this.talkStarted = true;
      this.messageIcon = 'fa-paper-plane';
    } else {
      this.talkStarted = false;
      this.messageIcon = 'fa-comment-alt';
    }
    if (this.visitorResponse !== undefined && this.visitorResponse !== null && this.visitorResponse !== '') {
      this.firebaseService.faqBotResponse = true;
      this.firebaseService.fetchFaqUserDetail(this.firebaseService.visitorCollectionTag, this.firebaseService.ipAddress);
      this.firebaseService.faqBotTalk.push(this.firebaseService.visitorMessage(this.visitorResponse, 'customer'));
      setTimeout(() => {
        this.fetchUserInfo()[this.firebaseService.faqBotLastTalk.visitorResponse] = this.visitorResponse;
        this.visitorResponse = undefined;
        this.firebaseService.faqBotWriteTalk = false;
        this.firebaseService.faqNextReplies(this.firebaseService.faqBotLastTalk);
        this.firebaseService.saveFaqUserDetail();
      }, 1000);
    }
  }

  quickReplyBtn(data: any, quick: any) {
    console.log('VisitorResponse: ' + this.visitorResponse);
    this.firebaseService.faqBotTalk.push(this.firebaseService.visitorMessage(quick.title, 'customer'));
    this.firebaseService.faqBotResponse = true;
    if (data.nextReplies !== undefined && data.nextReplies !== null && data.nextReplies.length > 0) {
      this.firebaseService.faqNextReplies(data);
    } else if (data.quickReplies !== undefined && data.quickReplies !== null && data.quickReplies.length > 0) {
      this.firebaseService.faqQuickReplies(quick);
    }
    data.quickReplies = [];
  }

  fetchUserInfo(): User {
    if (this.firebaseService.visitorInfo === undefined || this.firebaseService.visitorInfo === null) {
      this.firebaseService.visitorInfo = {
        ipAddress: this.firebaseService.ipAddress,
        id: this.firebaseService.create_UUID(),
        name: undefined,
        senderId: 'bot'
      };
      this.firebaseService.visitorInfoList.push(this.firebaseService.visitorInfo);
    } else {
      const index = this.firebaseService.visitorInfoList.findIndex((r: any) => r.id === this.firebaseService.visitorInfo.id);
      this.firebaseService.visitorInfoList[index] = this.firebaseService.visitorInfo;
    }
    return this.firebaseService.visitorInfo;
  }
}

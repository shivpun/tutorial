export interface Message {
    id: string;
    ipAddress: string;
    type: string;
    content: string;
    senderId?: string;
    sender?: string;
    quickReplies?: QuickReply[];
    timeSent?: number;
    count?: number;
}

export interface Conversation {
    name: string;
    mode: string;
    content: string;
    quickReplies: QuickReply[];
}

export interface QuickReply {
    title: string;
    type: string;
    payload: string;
}

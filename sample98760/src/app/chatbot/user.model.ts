export interface User {
    id: string;
    ipAddress: string;
    name?: string;
    email?: string;
    phone?: string;
    query?: string;
    senderId: string;
}

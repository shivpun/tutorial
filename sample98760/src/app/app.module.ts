import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MetaModule } from '@ngx-meta/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductCreateComponent } from './product/product-create/product-create.component';
import { ProductNavigationComponent } from './product/product-navigation/product-navigation.component';
import { ProductCreateService } from './product/product-create/product-create.service';
import { WeboxappsComponent } from './weboxapps/weboxapps.component';
import { ChatbotComponent } from './chatbot/chatbot.component';

import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from 'src/environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    ProductCreateComponent,
    ProductNavigationComponent,
    ChatbotComponent,
    WeboxappsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MetaModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, 'webboxApps'),
    AngularFireDatabaseModule,
    AngularFirestoreModule, // Only required for database features
    AngularFireAuthModule, // Only required for auth features,
    AngularFireStorageModule, // Only required for storage features
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [ProductCreateService],
  bootstrap: [AppComponent]
})
export class AppModule { }

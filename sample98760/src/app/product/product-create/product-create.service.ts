import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ProductModel } from '../model/product.model';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { ProductResponse } from '../model/product-response.model';


@Injectable({
  providedIn: 'root'
})
export class ProductCreateService {

  constructor(private http: HttpClient) { }

  create(data: any): Observable<any> {
    const headers = new HttpHeaders()
    .set('Content-Type', 'application/json');
    const createUrl = 'http://192.168.1.14:8080' + '/product/create';
    const value = JSON.stringify(data);
    console.log(value + ' | ' + createUrl);
    return this.http.post(createUrl, value, {headers});
  }

  extractData(res: any) {
    const productResponse = res as ProductResponse;
    return productResponse;
  }

  handleErrorPromise(error: Response | any) {
    console.error(error.message || error);
    return Promise.reject(error.message || error);
  }

   // Error handling
   errorHandl(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
 }
}

import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ProductModel } from '../model/product.model';
import { ProductCreateService } from './product-create.service';
import { ProductResponse } from '../model/product-response.model';
declare var $: any;

@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProductCreateComponent implements OnInit {

  product: ProductModel = new ProductModel('', 'Sale,Expense,Rental');
  productResponse: ProductResponse;
  constructor(
    private productCreateService: ProductCreateService
  ) { }

  ngOnInit() {
    $('.chip.product-chip').on('click', function() {
      $(this).toggleClass('active');
    });
  }

  save() {
    console.log(this.product);
    this.productCreateService.create(this.product).subscribe((res: any) => {
      this.productResponse = res as ProductResponse;
    });
  }

  removeAlert(productRes: ProductResponse) {
    this.productResponse = undefined;
  }

  setProductTags(tag: any) {
    this.product.process(tag);
    console.log('Product Tag: ' + this.product.getTag());
  }

  alertProductResponse() {
    if (this.productResponse !== undefined) {
      return true;
    }
    return false;
  }
}

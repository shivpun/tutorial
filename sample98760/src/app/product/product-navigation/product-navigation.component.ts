import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-product-navigation',
  templateUrl: './product-navigation.component.html',
  styleUrls: ['./product-navigation.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProductNavigationComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

export class ProductModel {
    name: string;
    tag: string;
    constructor(
        name: string = '',
        tag: string = '') {
            this.name = name;
            this.tag = tag;
        }

    addTags(tags: string) {
        if (this.tag === '') {
            this.tag = tags;
        } else {
            this.tag = this.tag + ',' + tags;
        }
    }

    removeTags(tags: string) {
        if (this.tag !== '') {
            const value: string [] = this.tag.split(',');
            const index = value.indexOf(tags);
            value.splice(index, 1);
            this.tag = value.filter(e => e !== tags).join(',');
        } else {
            console.log('Remove Tags: ' + tags + ' ||' + this.tag);
        }
    }

    process(tags: string) {
        const value: string [] = this.tag.split(',');
        if (value.indexOf(tags) > -1) {
            this.removeTags(tags);
        } else {
            this.addTags(tags);
        }
    }

    getTag() {
        return this.tag;
    }
}

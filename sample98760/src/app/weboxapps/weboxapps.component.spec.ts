import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeboxappsComponent } from './weboxapps.component';

describe('WeboxappsComponent', () => {
  let component: WeboxappsComponent;
  let fixture: ComponentFixture<WeboxappsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeboxappsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeboxappsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

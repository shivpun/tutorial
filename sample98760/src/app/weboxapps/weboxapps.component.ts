import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-weboxapps',
  templateUrl: './weboxapps.component.html',
  styleUrls: [
    './weboxapps.component.scss'
  ],
  encapsulation: ViewEncapsulation.None
})
export class WeboxappsComponent implements OnInit {

  constructor(private titleService: Title, private metaService: Meta) { }

  ngOnInit() {
    // tslint:disable-next-line: max-line-length
    this.titleService.setTitle('Weboxapps | Business Solutions | Designing | Deployment | Monitoring | Maintenance of the Linux and Open Source based Environment');
    this.metaService.addTag({ name: 'twitter:card', content: 'summary_large_image' });
    this.metaService.addTag({ name: 'twistter:card', content: 'summary_large_image' });
  }

}

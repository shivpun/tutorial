// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyCMMhi-t53APOGbghreRg4MYUSOQolIvCI',
    authDomain: 'sample-98760.firebaseapp.com',
    databaseURL: 'https://sample-98760.firebaseio.com',
    projectId: 'sample-98760',
    storageBucket: 'sample-98760.appspot.com',
    messagingSenderId: '202142653941',
    appId: '1:202142653941:web:daf8d994419b8f990954bc',
    measurementId: 'G-38TYKYN530'
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

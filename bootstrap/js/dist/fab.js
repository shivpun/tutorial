/*!
  * Bootstrap fab.js v4.3.1 (https://getbootstrap.com/)
  * Copyright 2011-2020 The Bootstrap Authors (https://github.com/twbs/bootstrap/graphs/contributors)
  * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
  */
(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory(require('./dom/data.js'), require('./dom/manipulator.js'), require('./dom/selector-engine.js')) :
  typeof define === 'function' && define.amd ? define(['./dom/data.js', './dom/manipulator.js', './dom/selector-engine.js'], factory) :
  (global = global || self, global.FAB = factory(global.Data, global.Manipulator, global.SelectorEngine));
}(this, (function (Data, Manipulator, SelectorEngine) { 'use strict';

  Data = Data && Data.hasOwnProperty('default') ? Data['default'] : Data;
  Manipulator = Manipulator && Manipulator.hasOwnProperty('default') ? Manipulator['default'] : Manipulator;
  SelectorEngine = SelectorEngine && SelectorEngine.hasOwnProperty('default') ? SelectorEngine['default'] : SelectorEngine;

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function ownKeys(object, enumerableOnly) {
    var keys = Object.keys(object);

    if (Object.getOwnPropertySymbols) {
      var symbols = Object.getOwnPropertySymbols(object);
      if (enumerableOnly) symbols = symbols.filter(function (sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
      keys.push.apply(keys, symbols);
    }

    return keys;
  }

  function _objectSpread2(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};

      if (i % 2) {
        ownKeys(Object(source), true).forEach(function (key) {
          _defineProperty(target, key, source[key]);
        });
      } else if (Object.getOwnPropertyDescriptors) {
        Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
      } else {
        ownKeys(Object(source)).forEach(function (key) {
          Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
        });
      }
    }

    return target;
  }

  /**
   * --------------------------------------------------------------------------
   * Bootstrap (v4.3.1): util/index.js
   * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
   * --------------------------------------------------------------------------
   */

  var toType = function toType(obj) {
    return {}.toString.call(obj).match(/\s([a-z]+)/i)[1].toLowerCase();
  };

  var isElement = function isElement(obj) {
    return (obj[0] || obj).nodeType;
  };

  var typeCheckConfig = function typeCheckConfig(componentName, config, configTypes) {
    Object.keys(configTypes).forEach(function (property) {
      var expectedTypes = configTypes[property];
      var value = config[property];
      var valueType = value && isElement(value) ? 'element' : toType(value);

      if (!new RegExp(expectedTypes).test(valueType)) {
        throw new Error(componentName.toUpperCase() + ": " + ("Option \"" + property + "\" provided type \"" + valueType + "\" ") + ("but expected type \"" + expectedTypes + "\"."));
      }
    });
  };

  var getjQuery = function getjQuery() {
    var _window = window,
        jQuery = _window.jQuery;

    if (jQuery && !document.body.hasAttribute('data-no-jquery')) {
      return jQuery;
    }

    return jQuery;
  };

  var getAnime = function getAnime() {
    return anime;
  };

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME = 'FAB';
  var VERSION = '4.3.1';
  var DATA_KEY = 'bs.FAB';
  var Default = {
    direction: 'top',
    hoverEnabled: true,
    toolbarEnabled: false
  };
  var DefaultType = {
    direction: 'string',
    hoverEnabled: 'boolean',
    toolbarEnabled: 'boolean'
  };
  var ClassName = {
    DIRECTION: 'direction-',
    ACTIVE: 'active',
    SHOW: 'show'
  };
  var Selector = {
    FAB: '.fixed-action-btn',
    FLOAT_BUTTON: '.btn-floating',
    DATA_TARGET: '[data-target="fab"]',
    FAB_BACKDROP: '<div class="fab-backdrop"></div>',
    FLOAT_BUTTON_LIST: 'ul'
  };
  /**
   * ------------------------------------------------------------------------
   * Class Definition
   * ------------------------------------------------------------------------
   */

  var FAB =
  /*#__PURE__*/
  function () {
    function FAB(element, config) {
      this.isOpen = false; // Protected

      this.element = element;
      this.config = this._getConfig(config);
      this.element.classList.add("" + ClassName.DIRECTION + this.config.direction);
      this.$menu = SelectorEngine.children(this.element, Selector.FLOAT_BUTTON_LIST)[0];
      this.$floatingBtns = SelectorEngine.find(Selector.FLOAT_BUTTON, this.$menu);
      this.$floatingBtnsReverse = SelectorEngine.reverse(this.$floatingBtns);

      this._setListeners();
    }

    var _proto = FAB.prototype;

    _proto._setListeners = function _setListeners() {
      var _this = this;

      this._handleFABClickBound = this._handleFABClick.bind(this);
      this._handleOpenBound = this._open.bind(this);
      this._handleCloseBound = this._close.bind(this);

      if (this.config.hoverEnabled) {
        ['mouseenter', 'touchstart'].forEach(function (event) {
          return _this.element.addEventListener(event, _this._handleOpenBound);
        });
        ['mouseleave', 'touchend'].forEach(function (event) {
          return _this.element.addEventListener(event, _this._handleCloseBound);
        });
      } else {
        this.element.addEventListener('click', this._handleFABClickBound);
      }
    };

    _proto._handleFABClick = function _handleFABClick() {
      if (this.isOpen) {
        this._close();
      } else {
        this._open();
      }
    };

    _proto._open = function _open() {
      if (this.isOpen) {
        return;
      }

      this._animateInFAB();

      this.isOpen = true;
    };

    _proto._close = function _close() {
      if (!this.isOpen) {
        return;
      }

      this._animateOutFAB();

      this.isOpen = false;
    };

    _proto._animateInFAB = function _animateInFAB() {
      var _this2 = this;

      this.element.classList.add("" + ClassName.ACTIVE);
      var time = 0;
      this.$floatingBtnsReverse.forEach(function (el) {
        getAnime()({
          targets: el,
          opacity: 1,
          scale: [0.4, 1],
          translateY: [_this2.offsetY, 0],
          translateX: [_this2.offsetX, 0],
          duration: 275,
          delay: time,
          easing: 'easeInOutQuad'
        });
        time += 40;
      });
    };

    _proto._animateOutFAB = function _animateOutFAB() {
      var _this3 = this;

      this.$floatingBtnsReverse.forEach(function (el) {
        getAnime().remove(el);
        getAnime()({
          targets: el,
          opacity: 0,
          scale: 0.4,
          translateY: _this3.offsetY,
          translateX: _this3.offsetX,
          duration: 175,
          easing: 'easeOutQuad',
          complete: function complete() {
            _this3.element.classList.remove("" + ClassName.ACTIVE);
          }
        });
      });
    };

    _proto._getConfig = function _getConfig(config) {
      config = _objectSpread2({}, Default, {}, config);
      config.hoverEnabled = Boolean(config.hoverEnabled); // Coerce string values

      config.toolbarEnabled = Boolean(config.toolbarEnabled); // Coerce string values

      typeCheckConfig(NAME, config, DefaultType);
      return config;
    };

    FAB.fabInterface = function fabInterface(element, config) {
      var data = Data.getData(element, DATA_KEY);

      var _config = _objectSpread2({}, Default, {}, Manipulator.getDataAttributes(element), {}, typeof config === 'object' && config ? config : {});

      if (!data) {
        data = new FAB(element, _config);
      }

      if (typeof config === 'string') {
        if (typeof data[config] === 'undefined') {
          throw new TypeError("No method named \"" + config + "\"");
        }

        data[config]();
      }
    } // Static
    ;

    FAB.jQueryInterface = function jQueryInterface(config) {
      return this.each(function () {
        FAB.fabInterface(this, config);
      });
    } // Getters
    ;

    FAB.getInstance = function getInstance(element) {
      return Data.getData(element, DATA_KEY);
    };

    _createClass(FAB, null, [{
      key: "VERSION",
      get: function get() {
        return VERSION;
      }
    }]);

    return FAB;
  }();

  var $ = getjQuery();
  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   * add .tab to jQuery only if jQuery is present
   */

  /* istanbul ignore if */

  if ($) {
    var JQUERY_NO_CONFLICT = $.fn[NAME];
    $.fn[NAME] = FAB.jQueryInterface;
    $.fn[NAME].Constructor = FAB;

    $.fn[NAME].noConflict = function () {
      $.fn[NAME] = JQUERY_NO_CONFLICT;
      return FAB.jQueryInterface;
    };
  }

  return FAB;

})));
//# sourceMappingURL=fab.js.map

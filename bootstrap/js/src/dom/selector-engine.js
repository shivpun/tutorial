/**
 * --------------------------------------------------------------------------
 * Bootstrap (v4.3.1): dom/selector-engine.js
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * --------------------------------------------------------------------------
 */

import { find as findFn, findOne, matches, closest } from './polyfill'
import { makeArray } from '../util/index'

/**
 * ------------------------------------------------------------------------
 * Constants
 * ------------------------------------------------------------------------
 */

const NODE_TEXT = 3
const idMatch = /^#[\w-]*$/, classMatch = /^\.[\w-]*$/, htmlMatch = /<.+>/, singlet = /^\w+$/;

const SelectorEngine = {
  
  matches(element, selector) {
    return matches.call(element, selector)
  },

  find(selector, element = document.documentElement) {
    return findFn.call(element, selector)
  },

  findBy(selector, context) {
    context = context || document;
    var elems = (classMatch.test(selector) ? context.getElementsByClassName(selector.slice(1)) : singlet.test(selector) ? context.getElementsByTagName(selector) : context.querySelectorAll(selector));
    return elems;
  },

  findOne(selector, element = document.documentElement) {
    return findOne.call(element, selector)
  },

  children(element, selector) {
    const children = makeArray(element.children)

    return children.filter(child => this.matches(child, selector))
  },

  reverse(elements) {
    let $el = []
    if (elements instanceof NodeList) {
      elements.forEach(element => {
        $el.push(element)
      });
    }
    return $el.reverse()
  },

  parents(element, selector) {
    const parents = []

    let ancestor = element.parentNode

    while (ancestor && ancestor.nodeType === Node.ELEMENT_NODE && ancestor.nodeType !== NODE_TEXT) {
      if (this.matches(ancestor, selector)) {
        parents.push(ancestor)
      }

      ancestor = ancestor.parentNode
    }

    return parents
  },

  closest(element, selector) {
    return closest.call(element, selector)
  },

  prev(element, selector) {
    const siblings = []

    let previous = element.previousSibling

    while (previous && previous.nodeType === Node.ELEMENT_NODE && previous.nodeType !== NODE_TEXT) {
      if (this.matches(previous, selector)) {
        siblings.push(previous)
      }

      previous = previous.previousSibling
    }

    return siblings
  }
}

export default SelectorEngine

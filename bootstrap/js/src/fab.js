/**
 * --------------------------------------------------------------------------
 * Bootstrap (v4.3.1): FAB.js
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * --------------------------------------------------------------------------
 */

import {
    getjQuery,
    typeCheckConfig,
    getAnime
  } from './util/index'
  import Data from './dom/data'
  import Manipulator from './dom/manipulator'
  import SelectorEngine from './dom/selector-engine'
  
  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */
  
  const NAME = 'FAB'
  const VERSION = '4.3.1'
  const DATA_KEY = 'bs.FAB'
  const EVENT_KEY = `.${DATA_KEY}`
  const DATA_API_KEY = '.data-api'
  
  const Default = {
    direction: 'top',
    hoverEnabled: true,
    toolbarEnabled: false
  }
  
  const DefaultType = {
    direction: 'string',
    hoverEnabled: 'boolean',
    toolbarEnabled: 'boolean'
  }
  
  const ClassName = {
    DIRECTION: 'direction-',
    ACTIVE: 'active',
    SHOW: 'show'
  }
  
  const Selector = {
    FAB: '.fixed-action-btn',
    FLOAT_BUTTON: '.btn-floating',
    DATA_TARGET: '[data-target="fab"]',
    FAB_BACKDROP: '<div class="fab-backdrop"></div>',
    FLOAT_BUTTON_LIST: 'ul'
  }

  const Transition = {
    TRANSITION_0: 'transform .2s',
    TRANSITION_1: 'transform .2s cubic-bezier(0.550, 0.085, 0.680, 0.530), background-color 0s linear .2s',
    TRANSITION_2: 'transform .2s cubic-bezier(0.550, 0.055, 0.675, 0.190)'
  }
  
  /**
   * ------------------------------------------------------------------------
   * Class Definition
   * ------------------------------------------------------------------------
   */

class FAB {
  constructor(element, config) {
    this.isOpen = false
    // Protected
    this.element = element
    this.config = this._getConfig(config)
    this.element.classList.add(`${ClassName.DIRECTION}`+this.config.direction)
    this.$menu = SelectorEngine.children(this.element, Selector.FLOAT_BUTTON_LIST)[0]
    this.$floatingBtns = SelectorEngine.find(Selector.FLOAT_BUTTON, this.$menu)
    this.$floatingBtnsReverse = SelectorEngine.reverse(this.$floatingBtns)
    this._setListeners()    
  }

  _setListeners() {
    this._handleFABClickBound = this._handleFABClick.bind(this);
    this._handleOpenBound = this._open.bind(this);
     this._handleCloseBound = this._close.bind(this);
    if (this.config.hoverEnabled) {
      ['mouseenter', 'touchstart'].forEach((event)=>this.element.addEventListener(event, this._handleOpenBound));
      ['mouseleave', 'touchend'].forEach((event)=>this.element.addEventListener(event, this._handleCloseBound));
    } else {
      this.element.addEventListener('click', this._handleFABClickBound);
    }
  }

  _handleFABClick() {
    if (this.isOpen) {
      this._close();
    } else {
      this._open();
    }
  }

  _open() {
    if(this.isOpen) {
      return;
    }
    this._animateInFAB()
    this.isOpen = true
  }

  _close() {
    if(!this.isOpen) {
      return;
    }
    this._animateOutFAB()
    this.isOpen = false
  }

  _animateInFAB() {
    this.element.classList.add(`${ClassName.ACTIVE}`)
    let time = 0;
    this.$floatingBtnsReverse.forEach((el) => {
      getAnime()({
        targets: el,
        opacity: 1,
        scale: [0.4, 1],
        translateY: [this.offsetY, 0],
        translateX: [this.offsetX, 0],
        duration: 275,
        delay: time,
        easing: 'easeInOutQuad'
      });
      time += 40;
    });
  }

  _animateOutFAB() {
    this.$floatingBtnsReverse.forEach((el) => {
      getAnime().remove(el);
      getAnime()({
        targets: el,
        opacity: 0,
        scale: 0.4,
        translateY: this.offsetY,
        translateX: this.offsetX,
        duration: 175,
        easing: 'easeOutQuad',
        complete: () => {
          this.element.classList.remove(`${ClassName.ACTIVE}`)
        }
      });
    });
  }

  _getConfig(config) {
    config = {
      ...Default,
      ...config
    }
    config.hoverEnabled = Boolean(config.hoverEnabled) // Coerce string values
    config.toolbarEnabled = Boolean(config.toolbarEnabled) // Coerce string values
    typeCheckConfig(NAME, config, DefaultType)
    return config
  }

  static fabInterface(element, config) {
    let data = Data.getData(element, DATA_KEY)
    const _config = {
      ...Default,
      ...Manipulator.getDataAttributes(element),
      ...typeof config === 'object' && config ? config : {}
    }
    if (!data) {
      data = new FAB(element, _config)
    }

    if (typeof config === 'string') {
      if (typeof data[config] === 'undefined') {
        throw new TypeError(`No method named "${config}"`)
      }

      data[config]()
    }
  }
  // Static

  static jQueryInterface(config) {
    return this.each(function () {
      FAB.fabInterface(this, config)
    })
  } 
  // Getters

  static get VERSION() {
    return VERSION
  }

  static getInstance(element) {
    return Data.getData(element, DATA_KEY)
  }
}
  
const $ = getjQuery()

  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   * add .tab to jQuery only if jQuery is present
   */
  /* istanbul ignore if */
if ($) {
  const JQUERY_NO_CONFLICT = $.fn[NAME]
  $.fn[NAME] = FAB.jQueryInterface
  $.fn[NAME].Constructor = FAB
  $.fn[NAME].noConflict = () => {
    $.fn[NAME] = JQUERY_NO_CONFLICT
    return FAB.jQueryInterface
  }
}
  
export default FAB
import { createApp } from 'vue'
import App from './App.vue'
import { routers } from './router/index'
/*
Ex-1
*/
// createApp(App).use(routers).mount('#app')

/*
Ex-2
*/
const app = createApp(App)
app.use(routers)
app.mount('#app')

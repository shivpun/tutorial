import { createRouter, createWebHistory } from 'vue-router'
import BaseTimer from '@/components/BaseTimer.vue'

const routes = [
  {
    name: 'count-down',
    path: '/',
    component: BaseTimer
  }
]

export const routers = createRouter({
  history: createWebHistory(),
  routes: routes
})

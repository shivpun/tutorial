# 01-topic-vue3-basic

## Feature of Vue 3

### Import of `Vue` exposed API method
- Vue 2
```
import Vue from 'vue';
```

- Vue 3
```
import { createApp } from 'vue';
```
### `Vue` mounting
- Vue 2
```
new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
```

- Vue 3
```
createApp(App)
  .use(router)
  .use(store)
  .mount('#app')
```

### `Vue` Vuex for state management
- Vue 2
```
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: { /* ... */ },
  mutations: { /* ... */ },
  actions: { /* ... */ },
  getters: { /* ... */ },
  modules: { /* ... */ },
});
```

- Vue 3
```
import { createStore } from 'vuex';

export default createStore({
  state: { /* ... */ },
  mutations: { /* ... */ },
  actions: { /* ... */ },
  getters: { /* ... */ },
  modules: { /* ... */ },
});
```

### `Vue` Routing
-Vue 2
```
import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

export default new VueRouter({
  routes: [{
    path: '/',
    name: 'HomePage',
    component: () => import('pages/home'),
  }]
});
```

- Vue 3
```
import {
  createRouter,
  createWebHistory,
} from 'vue-router';

Vue.use(VueRouter);

export default createRouter({
  history: createWebHistory(),
  routes: [{
    path: '/',
    name: 'HomePage',
    component: () => import('pages/home'),
  }]
});
```

### `Vue` Multiple Root Element
- Vue 2
In Vue 2, there is always one root element i.e. `div` tag inside that complete template. Its similar to `React.Fragment` in React application
```
<template>
	<div>
		<h1>First</h1>
		<h2>Second</h2>
	<div>
</template>
```

- Vue 3
In Vue 3, its not necessary to have one root element i.e. we can remove `div` tag.
```
<template>
	<h1>First</h1>
	<h2>Second</h2>
</template>
```
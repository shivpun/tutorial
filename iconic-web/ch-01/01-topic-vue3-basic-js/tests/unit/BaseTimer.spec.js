import { shallowMount } from '@vue/test-utils'
import BaseTimer from '@/components/BaseTimer.vue'

describe('BaseTimer.vue', () => {
  /*
    On Mount:
      - mount means test will mount component and its child components
    On ShallowMount:
      - shallowMount will mount only component and stub its child components

    Mount for unit test. ShallowMount for e2e test
  */
  it('check defaultValue props.timePassed when passed', () => {
    // https://vue-test-utils.vuejs.org/api/wrapper/setdata.html
    const wrapper = shallowMount(BaseTimer)
    // console.log(wrapper.vm);
    expect(wrapper.vm.timePassed).toBe(0)
  //  expect(wrapper.vm.timerInterval).toBeNull();
  })

  it('BaseTimer exists', () => {
    const wrapper = shallowMount(BaseTimer)
    expect(wrapper.exists()).toBeTruthy()
  })
})

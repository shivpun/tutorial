import {
    Rule, chain, filter, apply,
    applyTemplates, Tree, SchematicsException, mergeWith, move,
    noop,  url
  } from '@angular-devkit/schematics';
import * as ts from '@schematics/angular/third_party/github.com/Microsoft/TypeScript/lib/typescript';
import {
  addDeclarationToModule,
} from '@schematics/angular/utility/ast-utils';
import { InsertChange } from '@schematics/angular/utility/change';
import { buildRelativePath, findModuleFromOptions } from '@schematics/angular/utility/find-module';
import { applyLintFix } from '@schematics/angular/utility/lint-fix';
import { parseName } from '@schematics/angular/utility/parse-name';
import { validateHtmlSelector, validateName } from '@schematics/angular/utility/validation';
import { buildDefaultPath, getWorkspace } from '@schematics/angular/utility/workspace';
import { MDBSchematicsSchema as MDBSchemaOptions} from './schema';
import { strings } from '@angular-devkit/core';

function readIntoSourceFile(host: Tree, modulePath: string): ts.SourceFile {
  const text = host.read(modulePath);
  if (text === null) {
    throw new SchematicsException(`File ${modulePath} does not exist.`);
  }
  const sourceText = text.toString('utf-8');
  return ts.createSourceFile(modulePath, sourceText, ts.ScriptTarget.Latest, true);
}

function addDeclarationToNgModule(options: MDBSchemaOptions): Rule {
  return (host: Tree) => {
    if (options.skipImport || !options.module) {
      return host;
    }
    options.type = !!options.type ? options.type : 'Component';
    const modulePath = options.module;
    const source = readIntoSourceFile(host, modulePath);
    const componentPath = `/${options.path}/`
                          + (options.flat ? '' : strings.dasherize(options.name) + '/')
                          + strings.dasherize(options.name)
                          + '.'
                          + strings.dasherize(options.type);
    const relativePath = buildRelativePath(modulePath, componentPath);
    const classifiedName = strings.classify(options.name) + strings.classify(options.type);
    const declarationChanges = addDeclarationToModule(source,
                                                      modulePath,
                                                      classifiedName,
                                                      relativePath);
    const declarationRecorder = host.beginUpdate(modulePath);
    for (const change of declarationChanges) {
      if (change instanceof InsertChange) {
        declarationRecorder.insertLeft(change.pos, change.toAdd);
      }
    }
    host.commitUpdate(declarationRecorder);
    //console.log(declarationChanges);
    //console.log(modulePath);
    return host;
  };
}

function buildSelector(options: MDBSchemaOptions, projectPrefix: string) {
  let selector = strings.dasherize(options.name);
  if (options.prefix) {
    selector = `${options.prefix}-${selector}`;
  } else if (options.prefix === undefined && projectPrefix) {
    selector = `${projectPrefix}-${selector}`;
  }
  return selector;
}

export default function(_options: MDBSchemaOptions): Rule {
  return async (host: Tree) => {
    const workspace = await getWorkspace(host);
    const project = workspace.projects.get(_options.project as string);
    if (_options.path === undefined && project) {
      _options.path = buildDefaultPath(project);
    }
    _options.module = findModuleFromOptions(host, _options);
    const parsedPath = parseName(_options.path as string, _options.name);
    const destPath = parsedPath.path + '/' + _options.name;
    _options.name = parsedPath.name;
    _options.path = parsedPath.path;
    _options.selector = _options.selector || buildSelector(_options, project && project.prefix || '');
    validateName(_options.name);
    validateHtmlSelector(_options.selector);
    const templatePath = './files/'+_options.component;
    const templateSource = apply(url(templatePath), [
      _options.skipTests ? filter(path => !path.endsWith('.spec.ts.template')) : noop(),
      _options.inlineStyle ? filter(path => !path.endsWith('.__style__.template')) : noop(),
      _options.inlineTemplate ? filter(path => !path.endsWith('.html.template')) : noop(),
      _options.datasource ? filter(path => !path.endsWith('.datasource.ts.template')) : noop(),
      applyTemplates({
        ...strings,
        'if-flat': (s: string) => _options.flat ? '' : s,
        ..._options,
      }),
      move(destPath),
    ]);
    return chain([
      addDeclarationToNgModule(_options),
      mergeWith(templateSource),
      _options.lintFix ? applyLintFix(_options.path) : noop(),
    ]);
  };
}
import { Rule } from '@angular-devkit/schematics';
import { MDBSchematicsSchema as MDBSchemaOptions } from './schema';
export default function (_options: MDBSchemaOptions): Rule;

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const schematics_1 = require("@angular-devkit/schematics");
const ts = require("@schematics/angular/third_party/github.com/Microsoft/TypeScript/lib/typescript");
const ast_utils_1 = require("@schematics/angular/utility/ast-utils");
const change_1 = require("@schematics/angular/utility/change");
const find_module_1 = require("@schematics/angular/utility/find-module");
const lint_fix_1 = require("@schematics/angular/utility/lint-fix");
const parse_name_1 = require("@schematics/angular/utility/parse-name");
const validation_1 = require("@schematics/angular/utility/validation");
const workspace_1 = require("@schematics/angular/utility/workspace");
const core_1 = require("@angular-devkit/core");
function readIntoSourceFile(host, modulePath) {
    const text = host.read(modulePath);
    if (text === null) {
        throw new schematics_1.SchematicsException(`File ${modulePath} does not exist.`);
    }
    const sourceText = text.toString('utf-8');
    return ts.createSourceFile(modulePath, sourceText, ts.ScriptTarget.Latest, true);
}
function addDeclarationToNgModule(options) {
    return (host) => {
        if (options.skipImport || !options.module) {
            return host;
        }
        options.type = !!options.type ? options.type : 'Component';
        const modulePath = options.module;
        const source = readIntoSourceFile(host, modulePath);
        const componentPath = `/${options.path}/`
            + (options.flat ? '' : core_1.strings.dasherize(options.name) + '/')
            + core_1.strings.dasherize(options.name)
            + '.'
            + core_1.strings.dasherize(options.type);
        const relativePath = find_module_1.buildRelativePath(modulePath, componentPath);
        const classifiedName = core_1.strings.classify(options.name) + core_1.strings.classify(options.type);
        const declarationChanges = ast_utils_1.addDeclarationToModule(source, modulePath, classifiedName, relativePath);
        const declarationRecorder = host.beginUpdate(modulePath);
        for (const change of declarationChanges) {
            if (change instanceof change_1.InsertChange) {
                declarationRecorder.insertLeft(change.pos, change.toAdd);
            }
        }
        host.commitUpdate(declarationRecorder);
        //console.log(declarationChanges);
        //console.log(modulePath);
        return host;
    };
}
function buildSelector(options, projectPrefix) {
    let selector = core_1.strings.dasherize(options.name);
    if (options.prefix) {
        selector = `${options.prefix}-${selector}`;
    }
    else if (options.prefix === undefined && projectPrefix) {
        selector = `${projectPrefix}-${selector}`;
    }
    return selector;
}
function default_1(_options) {
    return (host) => __awaiter(this, void 0, void 0, function* () {
        const workspace = yield workspace_1.getWorkspace(host);
        const project = workspace.projects.get(_options.project);
        if (_options.path === undefined && project) {
            _options.path = workspace_1.buildDefaultPath(project);
        }
        _options.module = find_module_1.findModuleFromOptions(host, _options);
        const parsedPath = parse_name_1.parseName(_options.path, _options.name);
        const destPath = parsedPath.path + '/' + _options.name;
        _options.name = parsedPath.name;
        _options.path = parsedPath.path;
        _options.selector = _options.selector || buildSelector(_options, project && project.prefix || '');
        validation_1.validateName(_options.name);
        validation_1.validateHtmlSelector(_options.selector);
        const templatePath = './files/' + _options.component;
        const templateSource = schematics_1.apply(schematics_1.url(templatePath), [
            _options.skipTests ? schematics_1.filter(path => !path.endsWith('.spec.ts.template')) : schematics_1.noop(),
            _options.inlineStyle ? schematics_1.filter(path => !path.endsWith('.__style__.template')) : schematics_1.noop(),
            _options.inlineTemplate ? schematics_1.filter(path => !path.endsWith('.html.template')) : schematics_1.noop(),
            _options.datasource ? schematics_1.filter(path => !path.endsWith('.datasource.ts.template')) : schematics_1.noop(),
            schematics_1.applyTemplates(Object.assign({}, core_1.strings, { 'if-flat': (s) => _options.flat ? '' : s }, _options)),
            schematics_1.move(destPath),
        ]);
        return schematics_1.chain([
            addDeclarationToNgModule(_options),
            schematics_1.mergeWith(templateSource),
            _options.lintFix ? lint_fix_1.applyLintFix(_options.path) : schematics_1.noop(),
        ]);
    });
}
exports.default = default_1;
//# sourceMappingURL=index.js.map
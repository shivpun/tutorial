import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-interpolation',
  template: `<h1>
                (msg)
             </h1>`,
  interpolation: ['(', ')']
})
export class InterpolationComponent implements OnInit {

  private msg = 'Hello World';

  constructor() { }

  ngOnInit() {
  }
}

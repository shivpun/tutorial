import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Accordion0Component } from './accordion0.component';

describe('Accordion0Component', () => {
  let component: Accordion0Component;
  let fixture: ComponentFixture<Accordion0Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Accordion0Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Accordion0Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

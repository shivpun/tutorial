import { Component, OnInit, ViewEncapsulation } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-accordion0',
  templateUrl: './accordion0.component.html',
  styleUrls: ['./accordion0.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class Accordion0Component implements OnInit {

  constructor() {
    $('.collapse').collapse('show');
  }

  ngOnInit() {
  }

}

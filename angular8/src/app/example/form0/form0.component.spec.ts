import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Form0Component } from './form0.component';

describe('Form0Component', () => {
  let component: Form0Component;
  let fixture: ComponentFixture<Form0Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Form0Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Form0Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

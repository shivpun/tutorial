import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-form0',
  templateUrl: './form0.component.html',
  styleUrls: ['./form0.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class Form0Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-hamburger1',
  templateUrl: './hamburger1.component.html',
  styleUrls: ['./hamburger1.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class Hamburger1Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

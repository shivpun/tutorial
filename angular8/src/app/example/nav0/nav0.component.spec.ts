import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Nav0Component } from './nav0.component';

describe('Nav0Component', () => {
  let component: Nav0Component;
  let fixture: ComponentFixture<Nav0Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Nav0Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Nav0Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-nav0',
  templateUrl: './nav0.component.html',
  styleUrls: ['./nav0.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class Nav0Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Toggle1Component } from './toggle1.component';

describe('Toggle1Component', () => {
  let component: Toggle1Component;
  let fixture: ComponentFixture<Toggle1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Toggle1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Toggle1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

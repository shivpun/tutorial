import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-toggle0',
  templateUrl: './toggle0.component.html',
  styleUrls: ['./toggle0.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class Toggle0Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

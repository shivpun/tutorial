import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Toggle0Component } from './toggle0.component';

describe('Toggle0Component', () => {
  let component: Toggle0Component;
  let fixture: ComponentFixture<Toggle0Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Toggle0Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Toggle0Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

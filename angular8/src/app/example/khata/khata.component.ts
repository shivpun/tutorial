import { Component, OnInit, ViewEncapsulation } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-khata',
  templateUrl: './khata.component.html',
  styleUrls: ['./khata.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class KhataComponent implements OnInit {

  constructor() {
    console.log('Inside KhataComponent contructor');
   }

  ngOnInit() {
    $('.fixed-action-btn').FAB({direction: 'left', toolbarEnabled: false, hoverEnabled: false});
    console.log('Inside KhataComponent ngOnInit');
  }

}

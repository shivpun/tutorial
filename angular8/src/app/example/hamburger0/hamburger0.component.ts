import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-hamburger0',
  templateUrl: './hamburger0.component.html',
  styleUrls: ['./hamburger0.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class Hamburger0Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

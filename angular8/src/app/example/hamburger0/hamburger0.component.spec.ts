import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Hamburger0Component } from './hamburger0.component';

describe('Hamburger0Component', () => {
  let component: Hamburger0Component;
  let fixture: ComponentFixture<Hamburger0Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Hamburger0Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Hamburger0Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

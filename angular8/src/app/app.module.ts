import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InterpolationComponent } from './interpolation/interpolation.component';
import { ProductComponent } from './product/product.component';
import { Accordion0Component } from './example/accordion0/accordion0.component';
import { Toggle0Component } from './example/toggle0/toggle0.component';
import { Toggle1Component } from './example/toggle1/toggle1.component';
import { Form0Component } from './example/form0/form0.component';
import { Nav0Component } from './example/nav0/nav0.component';
import { Hamburger0Component } from './example/hamburger0/hamburger0.component';
import { KhataComponent } from './example/khata/khata.component';
import { Hamburger1Component } from './example/hamburger1/hamburger1.component';

@NgModule({
  declarations: [
    AppComponent,
    InterpolationComponent,
    ProductComponent,
    Accordion0Component,
    Toggle0Component,
    Toggle1Component,
    Form0Component,
    Nav0Component,
    Hamburger0Component,
    KhataComponent,
    Hamburger1Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

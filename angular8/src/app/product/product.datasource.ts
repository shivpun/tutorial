import { DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge } from 'rxjs';

// TODO: Replace this with your own data model type
export interface ProductItem {
    id: number;
    name: string;
    price: number;
    active: boolean;
    code: string;
    category: string;
    select: boolean;
}

// TODO: replace this with real data from your application
const Product_Item_DATA: ProductItem[] = [
  {id: 1,  name: 'Amul Milk', price: 22, active:true, code:'001', category: 'Dairy', select: false},
  {id: 2,  name: 'Train Ticket', price: 10, active:true, code: '002', category: 'Tour & Travel', select: false},
  {id: 3,  name: 'Vada Pav', price: 14, active: true, code: '003', category: 'Snacks', select: false},
  {id: 4,  name: 'Hydrogen', price: 0, active: true, code: '004', category: '', select: false},
  {id: 5,  name: 'Hydrogen', price: 0, active: true, code: '004', category: '', select: false},
  {id: 6,  name: 'Hydrogen', price: 0, active: true, code: '004', category: '', select: false},
  {id: 7,  name: 'Hydrogen', price: 0, active: true, code: '004', category: '', select: false},
  {id: 8,  name: 'Hydrogen', price: 0, active: true, code: '004', category: '', select: false},
  {id: 9,  name: 'Hydrogen', price: 0, active: true, code: '004', category: '', select: false},
  {id: 10, name: 'Hydrogen', price: 0, active: true, code: '004', category: '', select: false},
  {id: 11, name: 'Hydrogen', price: 0, active: true, code: '004', category: '', select: false},
  {id: 12, name: 'Hydrogen', price: 0, active: true, code: '004', category: '', select: false},
  {id: 13, name: 'Hydrogen', price: 0, active: true, code: '004', category: '', select: false},
  {id: 14, name: 'Hydrogen', price: 0, active: true, code: '004', category: '', select: false},
  {id: 15, name: 'Hydrogen', price: 0, active: true, code: '004', category: '', select: false},
  {id: 16, name: 'Hydrogen', price: 0, active: true, code: '004', category: '', select: false},
  {id: 17, name: 'Hydrogen', price: 0, active: true, code: '004', category: '', select: false},
  {id: 18, name: 'Hydrogen', price: 0, active: true, code: '004', category: '', select: false},
  {id: 19, name: 'Hydrogen', price: 0, active: true, code: '004', category: '', select: false},
  {id: 20, name: 'Hydrogen', price: 0, active: true, code: '004', category: '', select: false},
  {id: 21, name: 'Hydrogen', price: 0, active: true, code: '004', category: '', select: false},
  {id: 22, name: 'Hydrogen', price: 0, active: true, code: '004', category: '', select: false},
  {id: 23, name: 'Hydrogen', price: 0, active: true, code: '004', category: '', select: false},
  {id: 24, name: 'Hydrogen', price: 0, active: true, code: '004', category: '', select: false},
  {id: 25, name: 'Hydrogen', price: 0, active: true, code: '004', category: '', select: false},
  {id: 26, name: 'Hydrogen', price: 0, active: true, code: '004', category: '', select: false},
  {id: 27, name: 'Hydrogen', price: 0, active: true, code: '004', category: '', select: false},
  {id: 28, name: 'Hydrogen', price: 0, active: true, code: '004', category: '', select: false},
  {id: 29, name: 'Hydrogen', price: 0, active: true, code: '004', category: '', select: false},
  {id: 30, name: 'Hydrogen', price: 0, active: true, code: '004', category: '', select: false},
];

/**
 * Data source for the Product view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class ProductDataSource extends DataSource<ProductItem> {
  data: ProductItem[] = Product_Item_DATA;
  paginator: MatPaginator;
  sort: MatSort;

  constructor() {
    super();
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<ProductItem[]> {
    // Combine everything that affects the rendered data into one update
    // stream for the data-table to consume.
    const dataMutations = [
      observableOf(this.data),
      this.paginator.page,
      this.sort.sortChange
    ];

    return merge(...dataMutations).pipe(map(() => {
      return this.getPagedData(this.getSortedData([...this.data]));
    }));
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {}

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: ProductItem[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: ProductItem[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      switch (this.sort.active) {
        case 'name': return compare(a.name, b.name, isAsc);
        case 'id': return compare(+a.id, +b.id, isAsc);
        default: return 0;
      }
    });
  }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}

import { AfterViewInit, Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ProductDataSource, ProductItem } from './product.datasource';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProductComponent implements AfterViewInit, OnInit {

  dataSource: ProductDataSource;
  products = new Array<ProductItem>();
  productColors = new Array<string>();
  cardColors = new Array<string>();

  constructor() {
  }

  ngOnInit() {
    this.dataSource = new ProductDataSource();
    this.products = this.dataSource.data;
  }

  ngAfterViewInit() {
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InterpolationComponent } from './interpolation/interpolation.component';
import { ProductComponent } from './product/product.component';
import { Accordion0Component } from './example/accordion0/accordion0.component';
import { Toggle0Component } from './example/toggle0/toggle0.component';
import { Toggle1Component } from './example/toggle1/toggle1.component';
import { Form0Component } from './example/form0/form0.component';
import { Nav0Component } from './example/nav0/nav0.component';
import { Hamburger0Component } from './example/hamburger0/hamburger0.component';
import { KhataComponent } from './example/khata/khata.component';
import { Hamburger1Component } from './example/hamburger1/hamburger1.component';

const routes: Routes = [
    {path: 'khata', component: KhataComponent},
    {path: 'accordion0', component: Accordion0Component},
    {path: 'hamburger0', component: Hamburger0Component},
    {path: 'hamburger1', component: Hamburger1Component},
    {path: 'nav0', component: Nav0Component},
    {path: 'form0', component: Form0Component},
    {path: 'toggle0', component: Toggle0Component},
    {path: 'toggle1', component: Toggle1Component},
    {path: 'interpolation', component: InterpolationComponent},
    {path: 'product', component: ProductComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

var app = angular.module('tutorial', ['ngRoute']);

app.run(function () {
    console.log('Tutorial angularJS run BLOCK');  
});

app.config(function($interpolateProvider) {
    console.log('Tutorial angularJS config BLOCK');
    $interpolateProvider.startSymbol('((');
    $interpolateProvider.endSymbol('))');
});

app.controller('myCtrl', ['$scope', function($scope) {
    $scope.msg = 'Hello World';
}]);


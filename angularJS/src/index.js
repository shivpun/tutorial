const express = require('express');
const path = require('path');
const app = express();
const request = require('request')

var options = {
    dotfiles: 'ignore',
    etag: false,
    extensions: ['htm', 'html', 'js', 'css'],
    index: false,
    maxAge: '1d',
    redirect: false,
    setHeaders: function (res, path, stat) {
      res.set('X-Timestamp', Date.now());
      res.set('X-Powered-By', 'AngularJS Tutorial');
    }
}

/*
Serve the static files.
*/
app.use('/external', express.static('node_modules', options));
app.use('/static', express.static('src'));
app.use('/', express.static('src/dashboard'));
app.use(function (req, res, next) {
    console.log('Time: %d', Date.now())
    next()
})

/*
Request Mapping
*/
app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/dashboard/index.html'));
});

/* 
app.listen(path, [callback])
or
app.listen([port[, host[, backlog]]][, callback])

For Examples:
1. For UNIX socket
app.listen('/tmp/sock');

2. Listens to specific port
app.listen(3000);

3. Perform action via function
app.listen(3000, function() {});
*/
app.listen(3000, function(){
    console.log('ExpressJS listen on port 3000 !!') ;
});


/*
var kafka = require('kafka-node'),
client = new kafka.KafkaClient({kafkaHost: 'localhost:9092'}),
Consumer = kafka.Consumer,
consumer = new Consumer(
    client,
    [
        { topic: 'filtered-option-chain', partition: 0 }
    ],
    {
      autoCommit: true,
      fetchMaxWaitMs: 1000,
      fetchMaxBytes: 1024 * 1024,
      encoding: 'buffer'
    }
);

client.on('connect', function() {
  console.log('Connected');
});

client.on('ready', function() {
  console.log('Ready');
});

consumer.on('message', function (message) {
  console.log(message);
});
 */

 /*
const { Kafka } = require('kafkajs')
const kafka = new Kafka({
  clientId: 'my-app',
  brokers: ['localhost:9092']
})

const consumer = kafka.consumer({ groupId: 'test-group' })

const run = async () => {

  await consumer.connect()
  await consumer.subscribe({ topic: 'filtered-option-chain', fromBeginning: true })

  await consumer.run({
    eachMessage: async ({ topic, partition, message }) => {
      console.log({
        partition,
        offset: message.offset,
        value: message.value.toString(),
      })
    },
  })
}

run().catch(console.error)
*/


const Telegraf = require('telegraf')

const bot = new Telegraf('1085456098:AAEBGh1npHEP53FIBOv9tJ4zXOpliP3c8tw')
bot.command('connect', (ctx) => ctx.reply('Hello World, How are you ?'))
// Naive authorization middleware
// bot.telegram.sendMessage(654713666, 'Welcome to FAQ Bot');
// bot.telegram.getChat(654713666).then(r=>console.log(r));
bot.on('text', (ctx) => {
  console.log(ctx.message);
  console.log('Customer: ' + ctx.message.text);
  
process.stdin.setEncoding('utf8');
let chunk;
process.stdin.on('readable', () => {
  
  // Use a loop to make sure we read all available data.
  while ((chunk = process.stdin.read()) !== null) {
    process.stdout.write(`Bot: ${chunk}`);
    ctx.reply(`${chunk}`);
    // bot.telegram.sendMessage(711483323, 'One of the Customer want to connect to you. Are you aviable');
  }
 
});

process.stdin.on('end', () => {
  process.stdout.write('end');
});

  
});
bot.telegram.sendMessage(711483323, 'One of the Customer want to connect to you. Are you aviable');
bot.command('modern', ({ reply }) => reply('Yo'))
bot.command('hipster', Telegraf.reply('λ'))

bot.launch()

//711483323 => Prasad 
//992715019 => RishabhMAy
//654713666 => Punit

/*
process.stdin.setEncoding('utf8');
let chunk;
process.stdin.on('readable', () => {
  
  // Use a loop to make sure we read all available data.
  while ((chunk = process.stdin.read()) !== null) {
    process.stdout.write(`Bot: ${chunk}`);
    // ctx.reply(`${chunk}`);
    bot.telegram.sendMessage(711483323, `${chunk}`);
  }
 
});
*/